---
date: [:VIM_EVAL:]strftime('%Y-%m-%d')[:END_EVAL:]
title:
subtitle:
---

<style>
  @page {
    margin: 2cm;
    margin-inside: 2cm;
    margin-outside: 2cm;
    prince-shrink-to-fit: auto;

    @top-right {
      content: counter(page);
    }
    @top-left {
      content: 'Nicholas Skoretz (0927127)'
    }
  }

  img {
    max-height: 900px;
  }

  .wc {
    page-break-before: always;
  }
</style>
