
" General settings
source $HOME/.config/nvim/config/general.vimrc

" Keybindings
source $HOME/.config/nvim/config/keys.vimrc

" Windows nvim settings
source $HOME/.config/nvim/config/windows.vimrc

" Plug init
source $HOME/.config/nvim/config/pluginit.vimrc

" Plugin configs
source $HOME/.config/nvim/config/plugconfig.vimrc
