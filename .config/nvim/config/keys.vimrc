" #######################
" Keybindings {{{
let mapleader = ","
    " Rebind leader key

noremap <Leader>rt :vs<CR><C-w>l:terminal<CR>
    " Open a Bash terminal in vertical split

noremap <Leader>cd :cd <C-r>*<CR>
    " Change the directory to the text in the clipboard
noremap <Leader>dc "%<CR>
    " Paste the current directory
noremap <Leader>sd :cd %:p:h<CR>
    " Changing the global directory to the directory of the open file.

noremap <Leader>v :tabe ~/.config/nvim/init.vim<CR>
    " Open the init.vim

:tnoremap <Esc> <C-\><C-n>
    " Remap <ESC> to leave terminal.
" }}}