
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" ###################
" Plugins {{{
call plug#begin('~/.vim/plugged')
" Vim Improvements {{{
" ALE (Asynchronous Lint Engine)
Plug 'dense-analysis/ale'
    " Linting framework

" BufOnly
Plug 'vim-scripts/BufOnly.vim'
    " Close all buffers but the open one

" CamelCaseMotion
Plug 'bkad/CamelCaseMotion'
    " Allows 'w,b,e keys and iw, ib, ie to follow camel case and underscore.

" ctrlp
Plug 'kien/ctrlp.vim'
    " Better fuzzy search

" nerdtree
Plug 'scrooloose/nerdtree'
    " File explorer

" scartch.vim
Plug 'mtth/scratch.vim'
    " Quick scratch buffer

" supertab
Plug 'ervandew/supertab'
    " Tab as autocomplete

" Tabmerge
Plug 'vim-scripts/Tabmerge'
    " Move tabs to windows

" tabular
Plug 'godlygeek/tabular'
    " Help keep things aligned and beautiful

" targets.vim
Plug 'wellle/targets.vim'
    " Allows for better selection

" vim-abolish
Plug 'tpope/vim-abolish'
    " Better Text correction and other things

" vim-bookmarks
Plug 'MattesGroeger/vim-bookmarks'
    " Allows global bookmarks and marks are more visible

" vim-choosewin
Plug 't9md/vim-choosewin'
    " Easy window selection

" vim-commentary
Plug 'tpope/vim-commentary'
    " Commenting shortcuts and help

" vim-easy-align
Plug 'junegunn/vim-easy-align'
    " Better alignment, lets see if its better than Tabularize

" vim-fugitive
Plug 'tpope/vim-fugitive'
    " Git integration

" vim-obsession
Plug 'tpope/vim-obsession'
    " Better mksessions that save more information and track all changes

" vim-peekaboo
Plug 'junegunn/vim-peekaboo'
    " Peek at your registers when making a register insert

" vim-repeat
Plug 'tpope/vim-repeat'
    " Allows more commands to be repeated with <.>

" vim-surround
Plug 'tpope/vim-surround'
    " Add and remove surrounding quotes and other marks

" vim-unimpaired
Plug 'tpope/vim-unimpaired'
    " Adds more binds for 'prev' and 'next' actions

" wildfire.vim
Plug 'gcmt/wildfire.vim'
    " Also allows for better selection with <Enter>

" ZoomWin
Plug 'vim-scripts/ZoomWin'
    " Allows Zooming into a window and then restoring them back to normal
" Vim Improvements }}}

" For Programming {{{
" ansible-vim
Plug 'pearofducks/ansible-vim'
    " Adds filetype, syntax highlighting, and indentation for Ansible, Jinja2, and host files.

" emmet-vim
Plug 'mattn/emmet-vim'
    " HTML and CSS improved workflow, allows saving snippets

" python-mode
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
    "Full suite for Python, I really just want better syntax highlighting

" vim-go
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
    " Go Langauge Server and wrapper

" vim-js
Plug 'yuezk/vim-js'
    " Modern JS highlighting

" vim-jsx-pretty
Plug 'MaxMEllon/vim-jsx-pretty'
    " React JSX highlighting

" vim-ps1
Plug 'PProvost/vim-ps1'
    " Syntax highlighting and folding for powershell files

" vim-terraform
Plug 'hashivim/vim-terraform'
    " Syntax support for .tf, .tfvars, .tfstate. Terraform commands as vim commands

" vim-vue
Plug 'posva/vim-vue'
    " Highlighting for Vue.js
" For Programming }}}

" For Writing {{{
" goyo.vim
Plug 'junegunn/goyo.vim'
    " Centers and prettifys text for easy writing

" limelight.vim
Plug 'junegunn/limelight.vim'
    " Dims unfocused paragraphs for hyperfocus-writing

" vim-pandoc
Plug 'vim-pandoc/vim-pandoc' | Plug 'vim-pandoc/vim-pandoc-syntax'
    " Pandoc FileType support and syntax highlighting

" vim-pencil
Plug 'reedes/vim-pencil'
    " Formatting for text

" unicode.vim
Plug 'chrisbra/unicode.vim'
    " Tools to help insert unicode characters
" For Writing }}}

" Visual {{{
" indentLine
Plug 'Yggdroot/indentLine'
    " Display the indentation levels as vertical line (spaces over tabs)

" seoul256.vim
Plug 'junegunn/seoul256.vim'
    " A pretty colourscheme by junegunn

" taboo.vim
Plug 'gcmt/taboo.vim'
    " Allows naming tabs and better display of the tabline

" vim-airline
Plug 'vim-airline/vim-airline'
    " Context line at the bottom of files that can be customized

" vim-bufferline
Plug 'bling/vim-bufferline'
    " Bufferline to see buffers if no tabs are open

" vim-css-color
Plug 'ap/vim-css-color'
    " Makes colours visible in vim when the CSS hex code is typed

" vim-gitgutter
Plug 'airblade/vim-gitgutter'
    " Git status in the gutter
" Visual }}}
call plug#end()
" Plugins }}}
