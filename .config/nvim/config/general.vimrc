" Settings {{{
" Backup {{{
set nobackup
set noswapfile
set nowritebackup
" Backup }}}

" Compatibility {{{
set confirm              " Ask to save instead of post error
set fileformat=unix
set fileformats=unix,dos
set hidden               " Allows switching out of unsaved buffers
set nocompatible         " Avoid using outdated 'vi' commands
set nostartofline        " Avoid 'vi' style reset to top of line after some actions
set wildmenu             " Allows better command line completion
" Compatibility }}}

" Searching {{{
set hlsearch
set ignorecase
set smartcase
" Searching }}}

" Skeleton Files / Templates {{{
" Upon the creation of a new Buffer with a specific filetype, vim will :r(ead) the contents of the
" skeleton file into the buffer.
if has("autocmd")
    augroup templates
        autocmd BufNewFile *.* silent! execute '0r $HOME/.vim/templates/skeleton.'.expand("<afile>:e")
        autocmd BufRead,BufNewFile *.res set filetype=text
        autocmd BufNewFile * %substitute#\[:VIM_EVAL:\]\(.\{-\}\)\[:END_EVAL:\]#\=eval(submatch(1))#ge
    augroup END
endif
" Skeleton Files / Templates }}}

" Spell Check {{{
set spelllang=en_ca,es
" Spell Check }}}

" Tabbing {{{
" Sets indentation for filetypes
autocmd   FileType   css        setlocal   autoindent   shiftwidth=2   softtabstop=2   expandtab
autocmd   FileType   html       setlocal   autoindent   shiftwidth=2   softtabstop=2   expandtab
autocmd   FileType   javascript       setlocal   autoindent   shiftwidth=4   softtabstop=4   expandtab
autocmd FileType json setlocal autoindent shiftwidth=0 softtabstop=0 noexpandtab
autocmd   FileType   markdown   setlocal   autoindent   shiftwidth=2   softtabstop=2   expandtab
autocmd   FileType   pandoc     setlocal   autoindent   shiftwidth=2   softtabstop=2   expandtab
autocmd FileType ruby setlocal autoindent shiftwidth=2 softtabstop=2 expandtab
autocmd   FileType   scss       setlocal   autoindent   shiftwidth=2   softtabstop=2   expandtab
autocmd   FileType   vue        setlocal   autoindent   shiftwidth=2   softtabstop=2   expandtab
autocmd   FileType   vue        syntax     sync         fromstart
autocmd   FileType   yaml       setlocal   tabstop=2    shiftwidth=2   softtabstop=2   expandtab
set autoindent
set backspace=indent,eol,start " Allows backspacing ofer an autoindent
set expandtab
set shiftwidth=4
set softtabstop=4
" Tabbing }}}

" Terminal {{{
autocmd TermOpen * setlocal wrap " Allow line wrapping in terminal
" Terminal }}}

" View {{{
autocmd BufRead * setlocal foldmethod=marker " Toggles the fold remembrance emulation
autocmd BufRead * normal zM
let g:vim_json_conceal = 0          " Disable quote concealing in JSON files
let g:vim_json_syntax_conceal = 0   " Disable quote concealing in JSON files
set cmdheight=2                     " Command line is 2 high. Avoids 'enter to continue' messages
set colorcolumn=100                 " A line at col 100, to tell me when my lines are over
set history=100                     " Shows command history
set laststatus=2                    " Always show the status line
set list listchars=tab:\|\ ,trail:· " Show trailing whitespace with a '·' char
set number                          " Linenumbering
set relativenumber                  " Line numbering relative to the cursor's line
set ruler                           " Shows col and line num in bottom right
set shortmess=a                     " Command line is 2 high. Avoids 'enter to continue' messages.
set showcmd                         " Shows partial commands in the lower bar while typing
set splitbelow                      " :sp(lit) opens windows below of the current
set splitright                      " :vs(plit) opens windows to the right of the current
set textwidth=99
syntax on                           " Turn syntax highlighting on
" For some filetypes, disable line wrapping.
autocmd FileType text setlocal textwidth=0

set cursorcolumn
set cursorline
" View }}}
" Settings }}}
