" ###########################
" Plugin Settings {{{

" ALE {{{
"Enable linters
let g:ale_linters = {
            \ 'python': ['pydocstyle', 'flake8']
            \}
"Tell ALE 'pandoc' files are 'markdown'
let g:ale_linter_aliases = {'pandoc': ['markdown']}

"Tell 'mdl' to use these options
let g:ale_markdown_mdl_options = '--ignore-front-matter --style ~/.mdlstyle.rb'

"Tell where pydocstyle executable is
let g:ale_python_pydocstyle_executable = '/home/skoretz/.local/bin/pydocstyle'

" Tell 'flake8' to use these options
let g:ale_python_flake8_options = '--max-line-length 100'

" Use location list
let g:ale_set_loclist = 1
" }}}

" ansible-vim {{{
augroup ansible_vim_fthosts
  autocmd!
  autocmd BufNewFile,BufRead hosts setfiletype yaml.ansible
augroup END
" }}}

" CamelCaseMotion {{{
call camelcasemotion#CreateMotionMappings('<leader>')

map <silent> w <Plug>CamelCaseMotion_w
map <silent> b <Plug>CamelCaseMotion_b
map <silent> e <Plug>CamelCaseMotion_e
map <silent> ge <Plug>CamelCaseMotion_ge
sunmap w
sunmap b
sunmap e
sunmap ge

omap <silent> iw <Plug>CamelCaseMotion_iw
xmap <silent> iw <Plug>CamelCaseMotion_iw
omap <silent> ib <Plug>CamelCaseMotion_ib
xmap <silent> ib <Plug>CamelCaseMotion_ib
omap <silent> ie <Plug>CamelCaseMotion_ie
xmap <silent> ie <Plug>CamelCaseMotion_ie
"}}}

" emmet-vim {{{
let g:user_emmet_leader_key='<C-a>'
"}}}

" goyo.vim {{{
function! s:goyo_enter()
    set noshowmode
    set noshowcmd
    set scrolloff=999
    Limelight
    set linebreak
    set wrap
    set nocursorline
    set nocursorcolumn
endfunction

function! s:goyo_leave()
    set showmode
    set showcmd
    set scrolloff=0
    Limelight!
    set nolinebreak
    set cursorline
    set cursorcolumn
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
" }}}

" indentLine {{{
let g:indentLine_setConceal = 1
let g:indentLine_concealcursor='nc'
" }}}

" NERDTree {{{
noremap <Leader>n :NERDTreeToggle<CR>
"}}}

" python-mode {{{
let g:pymode_lint_on_write = 0
let g:pymode_lint = 0
" }}}

" seoul256 {{{
colorscheme seoul256 " Set colorscheme
" }}}

" taboo.vim {{{
set sessionoptions+=tabpages,globals
let g:taboo_tab_format="| %N.%f%m "
let g:taboo_renamed_tab_format="| %N.%l%m "
let g:taboo_modified_tab_flag="{!}"
" }}}

" tabular {{{
" Tabularize on colon
noremap <Leader>t5 :Tabularize/:<CR>
" Tabularize on comma
noremap <Leader>t6 :Tabularize/,<CR>
" Tabularize on semi-colon
noremap <Leader>t7 :Tabularize/;<CR>

noremap <Leader>t8 :Tabularize/=<CR>

noremap <Leader>t4 <CR>:Tabularize/<Bar><CR>
"}}}

" unicode.vim {{{
" UnicodeName shortcut for char under cursor
noremap <Leader>un :UnicodeName<CR>

"New Digraphs
"DigraphNew '\ 02c8
"DigraphNew :\ 02d0
"DigraphNew r\ 027e
"DigraphNew e\ 03b5
"DigraphNew o\ 0254
"DigraphNew t\ 02a6
"DigraphNew b\ 03b2
"DigraphNew y\ 03bb
"DigraphNew s\ 0283
"DigraphNew T\ 02a7
"DigraphNew n\ 0272
"DigraphNew z\ 01b7
"DigraphNew g\ 03b3
"DigraphNew d\ 00f0
"DigraphNew D\ 02a3
"DigraphNew j\ 029d
"DigraphNew h\ 03b8
"DigraphNew x\ 03c7
"DigraphNew w\ 02b7
" }}}

" vim-abolish {{{
let g:abolish_save_file = '$HOME/.config/nvim/after/plugin/abolish.vim'
" }}}

" vim-airline {{{
" Enable vim-bufferline integration
let g:airline#extensions#bufferline#enabled = 1
"}}}

" vim-bufferline {{{
let g:bufferline_echo = 0
let g:bufferline_active_buffer_left = '['
let g:bufferline_active_buffer_right = ']'
" }}}

" vim-choosewin {{{
nmap - <Plug>(choosewin)
"}}}

" vim-easy-align {{{
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
" }}}

" vim-go {{{
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
" }}}

" vim-gitgutter {{{
set updatetime=100

set signcolumn=yes
" }}}

" vim-pandoc {{{
let g:pandoc#syntax#conceal#use = 0
let g:pandoc#syntax#conceal#blacklist = ["quotes", "inlinecode"]
" let g:pandoc#syntax#conceal#cchar_overrides =
" let g:pandoc#spell#enabled = 1

""}}}

" vim-pencil {{{
let g:pencil#wrapModeDefault = 'soft'
" }}}

" vim-terraform {{{
let g:terraform_align=1
let g:terraform_fold_sections=1
let g:terraform_fmt_on_save=1
" }}}
" }}}
