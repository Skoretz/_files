" Abolish abbreviations

Abolish abt about
Abolish amt amount
Abolish algo{,s,ic} algorithm{,s,ic}

Abolish bw between

Abolish charac{,s} characteristic{,s}
Abolish comm{,s} communication{,s}

Abolish defn{,s} definition{,s}
Abolish dept{,s,al} department{,s,al}
Abolish diff{,s} differen{t,ces}

Abolish functoin{,s} function{,s}

Abolish iff if and only if
Abolish incerase increase

Abolish ling{,s} linguistic{,s}

Abolish mgmt management

" Abolish o of
Abolish ot other

Abolish prem premises

Abolish soln{,s} solution{,s}
Abolish spec{,s} specification{,s}
Abolish sw software

Abolish t the
Abolish thru through
Abolish tt that

Abolish w with
Abolish wd{,s} word{,s}
Abolish wh which
Abolish wn when
Abolish wt what
