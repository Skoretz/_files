#!/bin/bash

# sync.sh
# Nicholas Skoretz <nskoretz@pm.me>
# 2020-02-14
# 2022-04-10
# Compares files to their counterparts on the system and keeps things up to date.

# Functions {{{
function msg {
    local stringIn="[.SYNC] $1"
    echo "$stringIn"
}
# }}}

# main
msg "[start] sync of /_files with their locations on the system."

op="$1"
if [[ ! "$op" =~ ^(push|pull)$ ]]; then
    msg "Proper usage is: ./sync.sh [push|pull]"
    msg "push: Push files from this repository to machine locations."
    msg "pull: Pull files from machine into this repository."
    exit 1
fi

# Add exclude rules in this heredoc for to exclude files from the rsync commands.
cat << EOF > ./.sync_excludes
plugged/
EOF

# Add dirs here to sync them
syncDirs=(".config/nvim/" ".vim/" ".bashrc" ".bash_aliases")

for dir in "${syncDirs[@]}"; do
    msg "syncing: $dir"
    if [ "$op" = "push" ]; then
        mkdir --parents "$HOME"/"$dir"
        rsync \
            --archive \
            --update \
            --verbose \
            --exclude-from=.sync_excludes \
            "$dir" "$HOME"/"$dir"
        retcode=$?
    else
        rsync \
            --archive \
            --verbose \
            --exclude-from=.sync_excludes \
            "$HOME"/"$dir" "$dir"
        retcode=$?
    fi

    if [ $retcode -ne 0 ]; then
        msg "[ERR] rsync failed"
        exit 1
    fi
done
msg "[done]"
